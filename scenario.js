module.exports = {
    calculate_scenario: async function (messages) {
        console.log("\n####### Calculando escenarios #######")

        const MongoClient = require('mongodb').MongoClient;
        const uri = "mongodb+srv://jlrz:aqn6WAc46pm5MT0n@clustergft-ul8wv.mongodb.net/error_matrix?test=true&w=majority";
        const client = new MongoClient(uri, {
            useUnifiedTopology: true
        });

        let base_response = {
            level: "WARNING",
            code: "MESSAGE NOT FOUND",
            message: "El mensaje de error no ha sido encontrado",
            timestamp: Date.now()
        }

        // analysis
        console.log("MENSAJES DE SERVICIOS: \n", messages)
        console.log("conectado a db. . .")
        let query = await new Promise(resolve => {
            client.connect(async function (err) {
                if (err) {
                    console.log("ERR", err)
                    return resolve(err)
                }

                let and_array = [];
                messages.forEach(element => {
                    and_array.push({
                        "root_causes.service": element.service,
                        "root_causes.codes": element.code
                    })
                });
                const collection = client.db("error_matrix").collection("scenario")
                const items = await collection.findOne({
                    $and: and_array
                })

                client.close();
                return resolve(items);
            });
        });

        console.log("<< Respuesta escenario: \n", query)
        if (query) {
            base_response.level = query.level
            base_response.code = query.code
            base_response.message = query.message_code + " | " + query.message
        }
        console.log("####### Fin calculo escenarios #######\n")
        return base_response
    }
}