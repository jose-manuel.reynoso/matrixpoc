var express = require("express");
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
const request = require('request');
var scenario = require("./scenario.js");

console.log("/////////// INICIO POC MATRIX ///////////")
console.log("/////////// ORQUESTACION ///////////")
let i = 0;
app.listen(3000, () => {
    console.log("PUERTO:3000");
});

app.post("/orquestacion", (req, response, next) => {
    console.log("\n\n////////////////////////////////////////////")
    console.log("LLego peticion a orquestacion " + ++i)
    const options = {
        url: 'http://localhost:3001/negocio',
        method: 'POST',
        body: req.body,
        json: true
    };

    console.log(">> Llamando a capa negocio >> ");
    request(options, async function (err, res, bus_body) {
        try {
            let messages_orchestration;
            if (err) {
                console.log("error en llamada a negocio", err)
                messages_orchestration = [{
                    service: "BUS_PAYMENT",
                    code: "NO_RESPONSE"
                }]
            } else {
                console.log("<< Respuesta de negocio << : \n", bus_body);
                messages_orchestration = [{
                    service: "BUS_PAYMENT",
                    code: bus_body.code
                }]
            }
            let resp = await scenario.calculate_scenario(messages_orchestration);
            response.json(resp);

        } catch (error) {
            console.error("error", error)
            response.json(error);
        }
    });
});