var express = require("express");
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var scenario = require("./scenario.js");

console.log("/////////// NEGOCIO ///////////")

app.listen(3001, () => {
    console.log("PUERTO:3001");
});

app.post("/negocio", async function (req, res, next) {
    console.log("\n\n////////////////////////////////////////////")
    console.log(">> LLego peticion a negocio")
    console.log(" . . . llamadas a AaaS . . . ")
    let response = await scenario.calculate_scenario(req.body.messages);
    res.json(response)
});